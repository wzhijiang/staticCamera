(function() {
	// 构建对象
	var c = new Camera({
		zIndex: 10000
	});

	// 设置url
	c.onshow = function() {
		c.setUrl("data.json");
	};

	// 照相后回调处理
	c.onCaptureUploadComplete = function(data) {
		if(data.success == true && data.image) {
			c.close();
			R.Msg.info(data.msg || '照相成功');
		} else {
			R.Msg.info(data.msg || '照相失败');
		}
	};
	setTimeout(function() {
		c.show(); // 弹窗
	}, 0);
})();