R.ns('Camera.Static', {
	config: {
		FACE_CHECK_VALUE: 10
	},
	uploadUrl: "data.json", // camera 照相之后的请求
	defaultImg: 'img/default-icon-180-240.png',
	defaultSuccessImg: 'img/succeed-default-icon-180-240.png',
	cameraIsLoading: false,
	_camera: {
		id: "camera4validate",
		create: function() {
			var me = Camera.Static;
			R.Flash.create({
				id: me._camera.id,
				width: 240,
				height: 260,
				swf: Constants.Camera.swf.lens,
				params: {
					wmode: "opaque"
				},
				callback: function(status) {
					if(status.success == true) {
						$('div.ui-dialog:has(object)').attr({
							style: 'transform:none;transition:none;'
						});
						var $abtns = $('.header-img a');
						R.Flash.get(me._camera.id, function(flash) {
							if(R.isFunction(flash.isReady)) {
								me.cameraFlash = flash;
								$($abtns[0]).off('click').on('click', function() {
									me.cameraFlash.showSettings();
								});
								var flag = flash.getCameraMuted();
								if(flag != false) {
									if(flag == null) {
										R.Dialog.alert('没有找到摄像头,请安装摄像头后刷新页面重试!', function() {
											window.location.reload(true);
										});
									} else {
										R.Msg.info("请点击允许以便正常使用!");
									}
									var $div = $('<div class="learn-tile-bg" style="z-index: 102; position: fixed; left: 0px; top: 0px; width: 100%; height: 100%; overflow: hidden; -webkit-user-select: none;" tabindex="0"></div>');
									$('body').append($div);
									// 获取摄像头点击的是允许还是拒绝
									window.CameraAllowedHandler = function(isAllowed) {
										if(isAllowed === false && flash.getCameraMuted() === false) { // 允许
											$div.remove();
										} else {
											R.Msg.info("请设置成允许方能正常使用!");
										}
									}
								} else {
									// flash 允许记住后flag=false
									if(me.cameraFlash && R.isFunction(me.cameraFlash.isReady)) {
										if(me.cameraIsLoading == false) {
											me.cameraIsLoading = true;
											setTimeout(function() {
												me.cameraIsLoading = false;
											}, 3000);
											$('html,body').animate({
												scrollTop: 1
											}, 'slow');
											me._camera.setUrl(me.uploadUrl);
											$('#btn-capture').removeClass('hidden');
											$('#btn-init').addClass('hidden');
										} else {
											R.Msg.info("请等待验证完成!");
										}
									} else {
										R.Msg.info("请等待相机加载完成!");
									}
								}
								return true;
							}
						});
						$('.header-img a').removeClass('hidden');
					} else {
						R.Dialog.alert("在加载flash时出现问题! 请检查浏览器是否支持flash插件.");
					}
				}
			});
		},
		setUrl: function(url) {
			var me = Camera.Static;
			if(me.cameraFlash) {
				me.cameraFlash.setUrl(url);
			}
		},
		captures: function() {
			var me = Camera.Static;
			if(me.cameraFlash) {
				me.cameraFlash.captures(me.config.FACE_CHECK_VALUE);
			}
		},
		capture: function() {
			var me = Camera.Static;
			if(me.cameraFlash) {
				me.cameraFlash.capture();
				me.faceResult();
			} else {
				R.Msg.info('未启用人像');
			}
		}
	},
	reCapture: function(flash) {
		var me = this;
		R.Dialog.confirm("验证失败,请重新验证!", "提示", function(btn) {
			me.idCardReader.resume();
			if(btn == 'yes') {
				setTimeout(function() {
					if(flash == me.cameraFlash) {
						// me.setImg(me.rootUrl + u.faceUrl);
						me.faceResult();
					}
					flash.captures(me.config.FACE_CHECK_VALUE);
				}, 1000);
			}
		});
	},
	setImg: function(url) {
		var me = this;
		if(me.imgTimer) {
			clearTimeout(me.imgTimer);
		}
		var $img = $('.header-photo-box img');
		$img.attr('src', url);
		me.imgTimer = setTimeout(function() {
			//			$img.attr('src', me.defaultImg);
		}, 3 * 1000);
	},
	faceResult: function() {
		var me = this;
		window.onCaptureUploadComplete = function(data) {
			setTimeout(function() {
				me.cameraIsLoading = false;
			}, 1000);
			if(data.success === true) {
				me.setImg(data.image);
				R.Msg.info(data.msg || '照相成功!');
			} else {
				R.Msg.info(data.msg || '照相失败!');
			}
		}
	},
	init: function() {
		var me = Camera.Static;
		me._camera.create();
	}
});

(function() {})();