;
(function($) {
	var defaultSettings = {
		id: "camera4captrue",
		title: "人脸识别",
		url: Constants.Camera.url,
		swf: Constants.Camera.swf.lens,
		preview: false, // 是否有注册照
		width: 585,
		height: 322,
		buttons: false,
		cancelDisplay: false,
		cancelValue: "关闭"
	};

	function content(cfg) {
		var content = [];
		content.push('<div class="">');
		if(cfg.grid == true) {
			content.push('	<div class="camera-list">');
			content.push('		<h2>选择列表</h2>');
			content.push('		<ul id="camera-list">');
			content.push('		</ul>');
			content.push('	</div>');
			content.push('	<div class="camera-img">');
			content.push('		<img src="img/default-icon-180-240.png"/>');
			content.push('	</div>');
		} else {
			content.push('<div class="hint">');
			content.push('	<ul class="nav-tabs">');
			content.push('		<li class="active">');
			content.push('			<a href="javascript:;">图像示例</a>');
			content.push('		</li>');
			content.push('		<li>');
			content.push('			<a href="javascript:;">注意事项</a>');
			content.push('		</li>');
			content.push('		<li>');
			content.push('			<a href="javascript:;">设置帮助</a>');
			content.push('		</li>');
			content.push('	</ul>');
			content.push('	<div class="tab-content">');
			content.push('		<div class="hint-img">');
			content.push('			<ul>');
			content.push('				<li><img src="img/hint-01.gif" width="85" height="119" /></li>');
			content.push('				<li><img src="img/hint-02.gif" width="85" height="119" /></li>');
			content.push('				<li><img src="img/hint-03.gif" width="85" height="119" /></li>');
			content.push('				<li><img src="img/hint-04.gif" width="85" height="119" /></li>');
			content.push('				<li><img src="img/hint-05.gif" width="85" height="119" /></li>');
			content.push('				<li><img src="img/hint-06.gif" width="85" height="119" /></li>');
			content.push('			</ul>');
			content.push('		</div>');
			content.push('		<div class="hint-reminder hides">');
			content.push('			<ol style="list-style-type:none;">');
			content.push('				<li>&nbsp1&nbsp.&nbsp请调整坐姿，使头像位于人脸识别区域内</li>');
			content.push('				<li>&nbsp2&nbsp.&nbsp视线正对摄像头；</li>');
			content.push('				<li>&nbsp3&nbsp.&nbsp调节亮度和摄像头焦距使人脸显示清晰；</li>');
			content.push('				<li>&nbsp4&nbsp.&nbsp环境光线尽可能的均匀，无曝光、欠曝光；</li>');
			content.push('				<li>&nbsp5&nbsp.&nbsp图像中人的眼镜不反光，并保证双眼可见；</li>');
			content.push('				<li>&nbsp6&nbsp.&nbsp无墨镜、口罩等物体遮挡面部；</li>');
			content.push('				<li>&nbsp7&nbsp.&nbsp保证人脸、俯仰角度在+15~-15度之间；</li>');
			content.push('			</ol>');
			content.push('		</div>');
			content.push('		<div class="hint-help hides">');
			content.push('			<p>');
			content.push('				如果摄像头视频出现允许提示框可以点击“弹窗设置”按钮在出现的设置窗 口中，选中“允许”和“记住”，关闭该设置窗口后，就不会再出现提示允许框(如下图所示)');
			content.push('			</p>');
			content.push('			<div class="hint-help-img"><img src="img/hint-set.gif" width="213" height="136" /></div>');
			content.push('		</div>');
			content.push('	</div>');
			content.push('</div>');
		}
		content.push('	<div class="' + (cfg.grid == true ? 'camera-photo' : 'header-img') + '">');
		content.push('		<div class="header-img-box"><div id="' + cfg.id + '"></div></div>');
		if(cfg.grid != true) {
			content.push('	<div class="header-link">');
			content.push('		<table>');
			content.push('			<tr>');
			content.push('				<td>');
			content.push('					<span><button id="camera-setting" disabled="disabled">设置</button></span>');
			content.push('				</td>');
			content.push('				<td>');
			content.push('					<span><button id="capture" disabled="disabled">拍照</button></span>');
			content.push('				</td>');
			content.push('			</tr>');
			content.push('		</table>');
			content.push('	</div>');
		}
		content.push('	</div>');
		content.push('</div>');
		return content.join('');
	}

	function getBtn(cfg) {
		var content = [];
		content.push('	<td>');
		content.push('		<span><button title="' + (cfg.title || "") + '">' + cfg.text + '</button></span>');
		content.push('	</td>');
		return content.join('');
	}

	window.Camera = function(cfg) {
		var me = this;
		me.isShow = false;
		me.isReady = false;
		me.cfg = $.extend({}, defaultSettings, cfg || {});
		if(me.cfg.grid == true) {
			me.cfg.width = 760;
			me.cfg.height = 260;
		}
		$.extend(me.cfg, {
			content: content(me.cfg),
			onshow: function() {
				R.Flash.create({
					id: me.cfg.id,
					swf: me.cfg.swf,
					width: 240,
					height: 260,
					params: {
						wmode: 'opaque',
						allowScriptAccess: 'always'
					},
					callback: function(status) {
						if(status.success == true) {
							$('div.ui-dialog:has(object)').attr({
								style: 'transform:none;transition:none;'
							});
							R.Flash.get(me.cfg.id, function(flash) {
								if(R.isFunction(flash.isReady)) {
									me.isReady = true;
									me.flash = flash;
									if(R.isFunction(me.flash.setResource)) {
										me.flash.setResource(window.LIB + "/camera/face.zip");
									}
									if(me.cfg.url) {
										me.setUrl(me.cfg.url);
									}
									if(me.onshow && R.isFunction(me.onshow)) {
										me.onshow();
									}
									$('div.ui-dialog:has(object)').focus();
									$('#camera-setting').off('click').on('click', function() {
										me.flash.showSettings();
									});
									$('#capture, #camera-setting').prop('disabled', false);
									$('#capture').off('click').on('click', function() {
										if(me.flash.getCameraMuted() == false) {
											if(me.cfg.preview == false) {
												me.flash.capture();
											} else {
												var checkValue = 60;
												if(me.cfg.checkValue && Number(me.cfg.checkValue) > 0) {
													checkValue = Number(me.cfg.checkValue);
												}
												me.flash.captures(checkValue);
											}
											$('#capture').addClass('min-loading').text("").prop('disabled', true);
										}
									});
									if(me.cfg.grid != true) {
										$('.hint .nav-tabs').off('click', 'li').on('click', 'li', function() {
											var $this = $(this);
											if(!$this.hasClass('active')) {
												$this.addClass('active').siblings('.active').removeClass('active');
												var index = $this.index();
												$('.hint .tab-content').children().hide().eq(index).stop().show();
											}
										});
									}
									if(me.cfg.onShow) {
										R.isFunction(me.cfg.onShow, me.flash);
									}
									if(me.cfg.autoCaptrue == true) {
										window.detectionFace = function(isLoading) {
											if(isLoading == false && !$('#capture').prop('disabled')) {
												if(!window.Ext) {
													R.Msg.info("检测到人脸! 自动拍照中..");
												} else {
													Cqcis.Msg.info("检测到人脸! 自动拍照中..");
												}
												$('#capture').trigger('click');
											}
										}
									}
									return true;
								}
							});
						} else {
							R.Dialog.alert("在加载flash时出现问题! 请检查浏览器是否支持flash插件.");
						}
					}
				});
			}
		});
		if(me.cfg.grid != true) {
			window.onCaptureUploadComplete = function(data) {
				setTimeout(function() {
					$('#capture').removeClass('min-loading').text("拍照").prop('disabled', false);
				}, 1000);
				if(me.onCaptureUploadComplete && R.isFunction(me.onCaptureUploadComplete)) {
					me.onCaptureUploadComplete(data);
				}
			};
		}
	}

	Camera.prototype = {
		show: function() {
			var me = this;
			me._onclose = this.cfg.onclose || $.noop;
			$.extend(me.cfg, {
				onclose: me.onclose,
				buttons: false
			});
			if(R.isFunction(me.beforeshow)) {
				me.beforeshow();
			}
			me.w = R.Win.show(me.cfg);
			R.isFunction(me.aftershow) && me.aftershow();
			me.isShow = true;
		},
		beforeshow: $.noop,
		onshow: $.noop, // flash加载完成才执行
		aftershow: $.noop,
		close: function() {
			var me = this;
			if(me.w) {
				R.isFunction(me.beforeclose) && me.beforeclose(); // 注: 若是调用 Camera 实例的 w.close() 这不会触发此方法
				me.w.close().remove();
			}
			me.isShow = false;
		},
		beforeclose: $.noop,
		onclose: this._onclose,
		setUrl: function(url) {
			if(this.isReady || this.isReady === true) {
				this.flash.setUrl(url);
			}
		},
		onCaptureUploadComplete: $.noop,
		addbtn: function(cfg) {
			cfg = cfg || {};
			cfg.text = cfg.text || '按钮';
			var $btn = $(getBtn(cfg));
			if(cfg.handler && R.isFunction(cfg.handler)) {
				$btn.on('click', 'button', cfg.handler);
			}
			$('.header-img tr:last').append($btn);
		},
		setImg: function(url) {
			$('.camera-img img').attr('src', url);
		}
	}
})(jQuery);